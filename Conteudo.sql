-- 73 questoes e 63%
/** Verificar sintaxis de criacao de tabela, constraints, views, chaves, alter tables
** Testar apagar tabelas, e dependencias de objetos
** Constraints estudar sintaxis e tipos etc...
*/
/*
1 - Using Structured Query Language (SQL) {Formas normais, ER, Chaves}
        Explain the relationship between a database and SQL
        {DDL, DML, DCL, Transaction Control},
        {Alias, Keyword(SELECT, FROM, WHERE, GROUP BY, etc), Clause(SELECT a, b), Expression(Select 'Meu '||'Nome' )
            , Statment(Two o more clause Ex. SELECT and FROM clause), Text Literal
        }
*/
/*
2 - Using Data Manipulation Language (DML) and Transaction Control Language (TCL)
        Describe the purpose of  DDL{Statment are used to make changes to data dictionary(Create, alter and drop objects
            ; analyze informations on table, index, cluster; grant and revoke privileges)}
        Describe the purpose of  DML {Manage data in relational database(INSERT, UPDATE, SELECT, DELETE, MERGE)}
        Use DML to manage data in tables
        Use TCL to manage transactions
*/
-- alter table "Drop colum" or "set unsed colum" and "drop unsed colum"
/*
3 - Using Basic SELECT statements
        Build a SELECT statement to retrieve data from an Oracle Database table {Selection => Single row (where); Projection => filter coluns; Joining;
        Date in oracle Length 7 bytes field(1-The Century; 2-The Year; 3-The Month; 4-The Day; 5-The Hour; 6-The Minute; 7-The Second)}
        Use the WHERE clause to the SELECT statement to filter query results
*/
SELECT
    emp_last,
    emp_first,
    salary/280
FROM
    employees
WHERE
    emp_job = 'Pilot'
ORDER BY 
    salary 
;
SELECT
    emp_last,
    emp_first,
    salary/280 AS SALARY_BLA
FROM
    employees
WHERE
    emp_job = 'Pilot'
ORDER BY 
    salary 
;
/*
4 - Defining Table Joins
        Describe the different types of joins and their features
        Use joins to retrieve data from multiple tables
        Use self joins
*/
/*
5 - Using Conversion Functions and Conditional Expressions
        Use the TO_CHAR, TO_NUMBER, and TO_DATE conversion functions
        Apply general functions and conditional expressions in a SELECT statement
*/
-- Default NLS_DATE_FORMAT 'DD-MON-YY' 
-- to_char(datetime,'fmt','nlsparam')
-- to_char(n,'fmt','nlsparam') -- number
SELECT TO_CHAR(4235.34,'FML999,999.99') -- $4,235.34
FROM dual;
-- to_number
SELECT TO_NUMBER('R$4,355.80','FML999,999.99') -- 4355.8
FROM dual;
-- to_date(char,'fmt','nlsparam')
SELECT TO_DATE('February 23, 2012, 2:23 P.M.','Month, dd, YYYY, hh:mi A.M.') -- 23-FEB-12
FROM dual;
SELECT TO_DATE(2456029,'J') -- 11-APR-12
FROM dual;
SELECT TO_DATE('February 23, 2012, 2:23 P.M.','fxMonth, dd, YYYY, hh:mi A.M.') -- 23-FEB-12
FROM dual;
SELECT TO_DATE(sysdate,'fmMonth, dd, YYYY, hh:mi A.M.') -- JULY 13 9:30 // Remove espaço TODO revisar
FROM dual;

-- Apply general functions and conditional expressions in a SELECT statement
-- CASE
CASE expr
    WHEN comp_1 THEN return_1;
    [WHEN comp_2 THEN return_2,...]
    ELSE return_else
END CASE
;
-- DECODE
DECODE(exp, search, result[search2, result2,...], default) ;
/*
6 - Displaying Data from Multiple Tables
        Use SELECT statements to access data from more than one table using equijoins and nonequijoins
        Join a table to itself by using a self-join
        View data that generally does not meet a join condition by using outer joins
*/
-- TODO revisar as formas e tipo de joins
-- NATURAL JOIN table2 t2; nao usa apelidos
-- JOIN table2 t2 USING(col_name) colum_name
-- INNER JOIN table2 t2 ON t1.col = t2.col
-- LEFT|RIGHT|FULL OUTER JOIN table2 t2 ON t1.col = t2.col
-- CROSS JOIN table2 t2
-- Use SELECT statements to access data from more than one table using equijoins and nonequijoins
-- * Qualify coium
-- View data that generally does not meet a join condition by using outer joins
-- ** LEFT OUTER JOIN and LEFT JOIN are equivalent
/*
7 - Using the Set Operators
        Use a set operator to combine multiple queries into a single query
        Control the order of rows returned
*/
-- Union; Union all; Intersect; Minus
/*
8 - Using DDL Statements to Create and Manage Tables
        Describe data types that are available for columns
        -- Long 2GB; Binary_float 32 bits; Binary_double 64 bits; Interval day to second;
        -- ROWID Base 64 String unique adress; UROWID Base 64 String logical addres on index organized table;
        Create a simple table
        -- CREATE TABLE AS SELECT - herda somente os not nulls contraints
        Create constraints for tables
        Describe how schema objects work
        Execute a basic SELECT statement
*/
-- Names: object 30 bytes, database name 8 bytes; database link 128 bytes
-- Must be A-Z, 1-9, _, $, #.
-- Namespace: 
/*
9 - Managing Objects with Data Dictionary Views
        Use the data dictionary views to research data on objects
        Query various data dictionary views
*/
-- DBA_; ALL_; USER_.
-- V$; GV$; 
/*
10 - Managing Schema Objects
        Manage constraints
        Create and maintain indexes including invisible indexes and multiple indexes on the same columns
        Drop columns and set column UNUSED
        Perform flashback operations
        Create and use external tables
*/
/*
11 - Using Data Definition Language (DDL)
        Describe the purpose of DDL 
        Use DDL to manage tables and their relationships
        Explain the theoretical and physical aspects of a relational database
*/
/*
12 - Defining SELECT Statements
        Identify the connection between an ERD and a database using SQL SELECT statements
*/
/*
13 - Restricting and Sorting Data
        Use the ORDER BY clause to sort SQL query results
            {If does not contain ORDER By(Rows will be returnded in the
            order they were inserted into table, but is not always the case); 
            Must always be the last clause; LONG or LOB not is possible;
            Sort asc null last(defautl), sort desc nulls first(default);
            Expression itself(alias numeric value) userful when compoud set operator(UNION, INTERSECT, MINUS);
            Alias can not be use on WHERE clause}
        Limit the rows that are retrieved by a query
            {DISTINCT | UNIQUE canot be used on LOB column
            WHERE clause: }
            {NOT column = 'Valor' diferente de column != 'Valor' }//TODO Verificar se é a mesma coisa.
        Sort the rows that are retrieved by a query
        Use ampersand substitution to restrict and sort output at runtime
            {& ==> Variable; SET VERIFY ON ou SET VERIFY OFF} // Podem ser utilizados em qualquer parte
*/
SET VERIFY ON; -- Ativa a utilização das variaveis
SELECT &column
from 
    &table
where
    column = '&value' 
ORDER BY &order;
---------
SELECT &&column 
/* 
    Pode ser utilizada em outro local não perde o valor depois da execução e não é diferente da com um '&' 
    TODO testar a consulta com um e dois &&
*/
from 
    &table
where
    column = '&value' 
ORDER BY &order;

DEFINE variable_name; -- TODO Verificar se precisa do ponto e virgula ou se da erro
UNDEFINE variable_name;
--==================================================
-- Use SQL row to limiting clause novos {FETCH FIRST and OFFSET} must be "AFTER" ORDER BY
--==================================================
SELECT apt_name, apt_abbr, act_name
FROM aircraft_fleet_v
WHERE rownum < 6
ORDER BY apt_name, act_name
;

SELECT apt_name, apt_abbr, act_name
FROM aircraft_fleet_v
ORDER BY apt_name, act_name
FETCH FIRST 5 ROWS ONLY -- Must be in the end // Verificar se pode utilizar sem o ORDER BY
;

SELECT apt_name, apt_abbr, act_name
FROM aircraft_fleet_v
ORDER BY apt_name, act_name
OFFSET 3 FETCH NEXT 4 ROWS ONLY -- Must be in the end // Verificar se pode utilizar sem o ORDER BY
;

SELECT apt_name, apt_abbr, act_name
FROM aircraft_fleet_v
ORDER BY apt_name DESC, act_name DESC -- Por estar o contrario vai pegar as ultimas
FETCH FIRST 5 ROWS ONLY -- Must be in the end // Verificar se pode utilizar sem o ORDER BY
;

SELECT apt_name, apt_abbr, act_name
FROM aircraft_fleet_v
ORDER BY apt_name, act_name
FETCH FIRST 5 ROWS ONLY WITH TIES -- TESTAR!
;

SELECT apt_name, apt_abbr, act_name
FROM aircraft_fleet_v
ORDER BY apt_name, act_name
FETCH FIRST 25 PERCENT ROWS ONLY -- TESTAR!
;
/*
14 - Using Single-Row Functions to Customize Output
        Use various types of functions available in SQL
        Use conversion functions
        Use character, number, and date and analytical (PERCENTILE_CONT, STDDEV, LAG, LEAD) functions in SELECT statements
*/
-- Use various types of functions available in SQL
    -- Five distinct type single row function{Numeric(IN NUM, OUT NUM), Character(IN cha, OUT char or NUM), Datetime(IN Datetime, OUT date or NUM), Conversion, General}
  --#### Numeric Funcion #####
SELECT ABS(-5) -- Absolute Value
FROM dual;
SELECT CEIL(2343.2) -- Menor Maior valor inteiro acima. Ex: 2344
FROM dual;
SELECT FLOOR(21.2) -- Maior Menor valor inteiro abaixo. Ex: 21
FROM dual;
SELECT ROUND(127.623, 1), -- 127.6
    ROUND(127.623), --128
    ROUND(127.623, -1)  --130
FROM dual;

SELECT TRUNC(21.2222, 2) -- Maior Menor valor inteiro abaixo. Ex: 21.22
FROM dual;

SELECT SIGN(1), -- {-1 if n<0},{0 if n=0},{1 if n>0} Integer
    SIGN(1) -- {-1 if n<0},{1 if n>=0 or m = NaN} } Float
FROM dual;

--#### Character Funcion #####
SELECT INITCAP('Jhon jones') -- Jhon Jones
FROM dual;
SELECT LOWER('Jhon jones') -- jhon jones // Tbm tem o UPPER
FROM dual;
SELECT UPPER(apt_name) apt_name -- ORLANDO
FROM airports;
SELECT LPAD('Page 1',14,'.') -- .........Page 1  // Tbm tem o RPAD TODO Estudar
FROM dual;
SELECT LTRIM('\----/DATA\----/','/\-') -- DATA\----/  // RTRIM e TRIM TODO Estudar
FROM dual;
SELECT SUBSTR('John is bla bla',3,8) 
FROM dual;
SELECT ASCII('G') -- 71 -- Retuns decimal
FROM dual;
SELECT LENGTH('1Z0-047') -- 7
FROM dual;

--#### DATE Funcion #####
SELECT TO_CHAR(ADD_MONTHS('10-MAR-11',1),'dd-mon-yy') -- 10-APR-11
FROM dual;
SELECT LAST_DAY('12-MAR-11') -- 31-MAR-11
FROM dual;
SELECT MONTHS_BETWEEN('02-JAN-12','04-JUN-12') -- -5.0645161
FROM dual;
SELECT NEXT_DAY('03-MAR-12','FRIDAY') -- 09-MAR-12
FROM dual;
SELECT TRUNC(sysdate), -- 09-MAR-12 00:00:00
    TRUNC(sysdate,'HH'), -- 09-MAR-12 21:00:00
    TRUNC(sysdate,'MM'), -- 01-MAR-12 00:00:00
    TRUNC(sysdate,'YY') -- 01-JAN-12 00:00:00
FROM dual;
-- RR Need century, Ex: 32 ==> 1932 acima de 50 a 99 desconta 100 anossu
-- YY Use current century, Ex: 32 ==> 2032
--#### TO_NUMBER Funcion #####
-- TO_NUMBER(expr, fmt, 'nlsparam')
SELECT TO_NUMBER('$4,355.80','FML999G990D00') -- 4355.8
FROM dual;

--#### TO_CHAR Funcion #####
-- TO_CHAR(datetime, fmt, 'nlsparam')
SELECT TO_CHAR(sysdate,'Day, Month, DD, YYYY') -- Saturday, April, 07, 2012
FROM dual;

--#### TO_DATE Funcion #####
-- TO_DATE(char, fmt, 'nlsparam')
SELECT TO_DATE('Saturday, April, 07, 2012','Day, Month, DD, YYYY') -- 07-APR-12
FROM dual;

--#### general Funcion #####
-- NVL(exp1,exp2)
SELECT NVL('','Value is Null') as xx,  -- Value is Null
    NVL(dummy,'Value is Null') xxx  -- X
FROM dual;

SELECT NULLIF(dummy,'X') as xx  -- "null"
FROM dual;

-- Use character, number, and date and analytical (PERCENTILE_CONT, STDDEV, LAG, LEAD) functions in SELECT statements

-- TODO revisar este conteudo e fazer testes olhar na docuemtacao oficial
-- Single row and agregate funcion
SELECT act_body_style, avg(avg_seats) avg_seats
FROM aircraft_types
GROUP BY act_body_style
;
-- analyct_funcion([arguments]) over (analytic_clause)
SELECT act_body_style, avg_seats, 
    avg(avg_seats) over(partition by act_body_style) avg_by_seats
FROM aircraft_types
;
SELECT act_body_style, avg_seats, 
    avg(avg_seats) over() avg_over_all
FROM aircraft_types
;

/* TODO Verificar documentacao verificar nulos
15 - Reporting Aggregated Data Using the Group Functions
        Describe the use of group functions
            * Single result
            * AVG; COUNT; MEDIAN; MIN; MAX; SUM
        Group data by using the GROUP BY clause
        Include or exclude grouped rows by using the HAVING clause
*/
-- Null values are ignored for all the agregate function, exeption count(*)
-- max two nested: ex. AVG(MAX())
/*
16 - Using Subqueries to Solve Queries
        Define subqueries
        Describe the types of problems subqueries can solve
        Describe the types of subqueries
        Use correlated subqueries
        Update and delete rows using correlated subqueries
        Use the EXISTS and NOT EXISTS operators
        Use the WITH clause
        Use single-row and multiple-row subqueries
*/
-- Subquery can be GROUP function; Subquery can not include ORDER BY; Subquery that return no rows evalue to NULL
-- Sigele row or scalar every location
-- Multiple row 
-- Correlated - Quando usado referenciado a outra query normalmente no where
-- In line views; subquery no from
-- Nested; subquery no where até 255 subquerys

-- Existes return true and can be faster;

-- With clause: TODO verificar quanto tempo fica disponibel na tabela temporaria

/*
17 - Manipulating Data
        Insert rows into a table -- NULL não funciona(ON NULL clause as default) mais usar o DEFALUT
        Update rows in a table
        Delete rows from a table -- FROM é opcional se for deletar tudo
        Control transactions; Commit; Rollback; Savepoint; Set transaction; Set constraint(TODO ESTUDAR)
*/
-- TRUNCATE DDL; TRUNCATE TABLE table_name; no generate undo
/*
18 - Creating Other Schema Objects
        Create simple and complex views with visible/invisible columns
        Create, maintain and use sequences
*/
/*
19 - Controlling User Access
        Differentiate system privileges from object privileges
        Grant privileges on tables and on a user
        Distinguish between privileges and roles
*/
/*
20 - Manipulating Large Data Sets
        Describe the features of multitable INSERTs
        Merge rows in a table
*/
-- Fontes
-- https://education.oracle.com/oracle-database-sql/pexam_1Z0-071
-- https://renatomsiqueira.com/impressoes-sobre-o-exame-1z0-071/
-- https://oraclepress.wordpress.com/certificacoes/12c-oca-sql/